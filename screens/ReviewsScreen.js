import React, { Component } from 'react';
import { style } from '../constants/styles';

import HeaderTop from '../components/Header';
import Bottom from '../components/Footer';
import { Container, Content, Text } from 'native-base';

const title = 'Reviews';


export default class Reviews extends Component {
  static navigationOptions = {
    title,
    ...style.header,
  };

  render() {
    return (
      <Container>
        <HeaderTop title={title} backButton />
        <Content>
          <Text>Reviews</Text>
        </Content>
        <Bottom />
      </Container>

    );
  }
}
