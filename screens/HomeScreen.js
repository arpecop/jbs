import React, { Component } from 'react';
import { AsyncStorage, View } from 'react-native';
import chunk from 'lodash/chunk';
import { Container, Content, Col, Grid, Icon, Text, Badge } from 'native-base';

import { style } from '../constants/styles.js';
import data from '../constants/data.json';
import HeaderTop from '../components/Header';

export default class Home extends Component {
  static navigationOptions = {
    title: 'Home',
    ...style.header,
    drawerLabel: 'Home',
    headerLeft: null,
  };
  state = {
    buttons: data.routes,
  };

  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('SignIN');
  };

  render() {
    const HomeItems = Object.values(this.state.buttons).filter((item) => {
      if (item.homescreen) {
        return item;
      }
    });
    return (
      <Container>
        <HeaderTop
          title="Home"
          backButton={false}
          navigation={this.props.navigation}
        />
        <Content>
          {chunk(Object.values(HomeItems), 3).map((chunkx, i) => (
            <Content key={i}>
              <Grid>
                {chunkx.map((value) => {
                  const Badgex = value.count ? (
                    <View
                      style={{
                        position: 'absolute',
                        zIndex: 1,
                        top: 15,
                      }}
                    >
                      <Badge
                        color={value.color ? value.color : '#2c3e50'}
                        style={{ marginLeft: 30 }}
                        onPress={() =>
                          this.props.navigation.navigate(value.destination)
                        }
                      >
                        <Text style={{ color: '#FFF', fontSize: 12 }}>
                          {value.count}
                        </Text>
                      </Badge>
                    </View>
                  ) : (
                    <View />
                  );
                  return (
                    <Col key={value.destination} style={style.child}>
                      {Badgex}
                      <Icon
                        name={value.icon}
                        fontSize="60"
                        style={{ fontSize: 60, color: '#2c3e50' }}
                        onPress={() =>
                          this.props.navigation.navigate(value.destination)
                        }
                      />
                      <Text style={{ color: '#2c3e50' }}>{value.text}</Text>
                    </Col>
                  );
                })}
              </Grid>
            </Content>
          ))}

          <Col key="logout" style={style.child}>
            <Icon
              name="md-home"
              fontSize="60"
              style={{ fontSize: 60, color: '#2c3e50' }}
              onPress={() => this._signOutAsync()}
            />
            <Text style={{ color: '#2c3e50' }}>Logout</Text>
          </Col>
        </Content>
      </Container>
    );
  }
}
