import React, { Component } from 'react';
import { style } from '../constants/styles';

import HeaderTop from '../components/Header';
import Bottom from '../components/Footer';
import { Container, Content } from 'native-base';

const title = 'Settings';


export default class Settings extends Component {
  static navigationOptions = {
    title,
    ...style.header,
  };

  render() {
    return (
      <Container>
        <HeaderTop title={title} backButton />
        <Content />
        <Bottom />
      </Container>

    );
  }
}
