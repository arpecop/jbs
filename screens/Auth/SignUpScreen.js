import React from "react";
import { View, TextInput, Image, Text } from "react-native";

import Amplify, { Auth } from "aws-amplify";
import { Icon } from "native-base";
import { Button } from "react-native-elements";
// import PhoneInput from 'react-native-phone-input';
import Toast from "react-native-easy-toast";
import PhoneInput from "react-native-phone-input";
import CountryPicker from "react-native-country-picker-modal";
import AWSConfig from "../../aws-exports";

import { style } from "../../constants/styles";

Amplify.configure(AWSConfig);

const title = " ";

export default class SignUP extends React.Component {
  static navigationOptions = {
    title,
    headerTitleStyle: {
      color: "white",
      textAlign: "center",
      alignSelf: "center",
      fontFamily: "Roboto_light"
    },
    headerStyle: {
      position: "absolute",
      backgroundColor: "transparent",
      zIndex: 100,
      top: 0,
      left: 0,
      right: 0
    },
    headerTintColor: "white"
  };

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      phone_number: ""
    };
    this.onPressFlag = this.onPressFlag.bind(this);
    this.selectCountry = this.selectCountry.bind(this);
    this.state = {
      cca2: "US"
    };
  }

  onPressFlag() {
    this.countryPicker.openModal();
  }

  selectCountry(country) {
    this.phone.selectCountry(country.cca2.toLowerCase());
    this.setState({ cca2: country.cca2 });
  }

  onChangeText(key, value) {
    this.setState({
      [key]: value
    });
  }
  signUp() {
    Auth.signUp({
      username: this.state.username,
      password: this.state.password,
      attributes: {
        // email: this.state.email,
        phone_number: this.state.phone_number
      }
    })
      .then(() => {
        const x = 1;
        this.props.navigation.navigate("ConfirmLogin", {
          username: this.state.username
        });
      })
      .catch(err =>
        this.refs.toastWithStyle.show(err.message ? err.message : err, "top", 5)
      );
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center"
        }}
      >
        <Toast
          ref="toastWithStyle"
          style={{ backgroundColor: "red" }}
          position="top"
        />
        <Image
          style={{
            backgroundColor: "#ccc",
            flex: 1,
            resizeMode: "cover",
            position: "absolute",
            width: "100%",
            height: "100%",
            justifyContent: "center"
          }}
          source={require("../../assets/gradient.png")}
        />
        <Icon
          name="md-lock"
          color="white"
          style={{ color: "white", fontSize: 35, margin: 30 }}
        />
        <TextInput
          onChangeText={value => this.onChangeText("username", value)}
          placeholder="Username"
          underlineColorAndroid="transparent"
          style={style.loginInut}
          autoCorrect={false}
        />
        <TextInput
          style={style.loginInut}
          placeholder="Password"
          onChangeText={value => this.onChangeText("password", value)}
          secureTextEntry
        />
   
        <PhoneInput
          style={ {
    borderColor: 'rgba(0, 0, 0, 0.5);',
    backgroundColor: 'rgba(0, 0, 0, 0.5);',
    width: '90%',
    padding: 5,
 
    borderWidth: 1,
    borderStyle: 'solid',
   
    borderRadius: 25,
    marginBottom: 10,
  }}
  textStyle={{   fontFamily: 'Roboto_light',
    color: 'white',  fontSize: 15,}}
          ref={ref => {
            this.phone = ref;
          }}
          onChangePhoneNumber={value => this.onChangeText("phone_number", value)}
          onPressFlag={this.onPressFlag}
        />

        <CountryPicker
          ref={ref => {
            this.countryPicker = ref;
          }}
          onChange={value => this.selectCountry(value)}
          translation="eng"
          cca2={this.state.cca2}
        >
          <View />
        </CountryPicker>

        <Button
          onPress={this.signUp.bind(this)}
          backgroundColor="#4cda64"
          buttonStyle={style.GreenButton}
          title="Sign me Up!"
        />
      </View>
    );
  }
}
