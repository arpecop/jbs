import React, { Component } from 'react';
import { View } from 'react-native';
import Expo from 'expo';
import Amplify, { Auth } from 'aws-amplify';

import {
  FormLabel,
  FormInput,
  FormValidationMessage,
} from 'react-native-elements';
import { style } from '../../constants/styles';
import { amplifyConfig } from '../../constants/amplifyConfig';

const title = 'Forgot Password';
Amplify.configure(amplifyConfig);

export default class SignUPscreen extends React.Component {
  static navigationOptions = {
    title,
    ...style.header,
  };
  componentWillMount() {
    Auth.signIn('1', 'password')
      .then(user => console.log(user))
      .catch(err => console.log(err));
  }

  render() {
    //   <Authenticator />
    return <View />;
  }
}

//    <Button onPress={() => this.props.navigation.navigate('App')} title="Go to main screen" />
//    <FormLabel>Email</FormLabel>
// <FormInput onChangeText={this._signup} />
// ;
