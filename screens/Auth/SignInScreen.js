import React, { Component } from 'react';
import { View, Text, Image, AsyncStorage, TextInput } from 'react-native';

import { withNavigation } from 'react-navigation';

import {
  FormLabel,
  Button,
  FormInput,
  FormValidationMessage,
} from 'react-native-elements';

import Amplify, { Auth } from 'aws-amplify';
import AWSConfig from '../../aws-exports';
import { style } from '../../constants/styles';

Amplify.configure(AWSConfig);

class SignINScreen extends React.Component {
  static navigationOptions = {
    header: null,
    ...style.header,
  };
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      confirmationCode: '',
      user: {},
      err: null,
    };
  }

  componentWillMount() {}
  _handlePressLogin() {
    this.props.navigation.navigate('App');
  }
  onChangeText(key, value) {
    this.setState({
      [key]: value,
    });
  }
  signIn() {
    const { username, password } = this.state;
    Auth.signIn(username, password)
      .then((user) => {
        this.setState({ user });
        this.props.navigation.navigate('Home');
        AsyncStorage.setItem('userToken', this.state.username);
      })
      .catch((err) => {
        this.setState({ err });
      });
  }
  confirmSignIn() {
    Auth.confirmSignIn(this.state.user, this.state.confirmationCode)
      .then(() => {
        this.props.navigation.navigate('Home');
      })
      .catch((err) => {
        this.setState({ err });
      });
  }
  render() {
    {
      this.state.err ? (
        <FormValidationMessage>
          {this.state.err.message ? this.state.err.message : this.state.err}{' '}
        </FormValidationMessage>
      ) : (
        <View />
      );
    }

    return (
      <View
        style={{
          flex: 1,
        }}
      >
        <Image
          style={{
            backgroundColor: '#ccc',
            flex: 1,
            resizeMode: 'cover',
            position: 'absolute',
            width: '100%',
            height: '100%',
            justifyContent: 'center',
          }}
          source={require('../../assets/gradient.png')}
        />
        <View
          style={{
            flex: 1,
          }}
        >
          <Image
            resizeMode="contain"
            source={require('../../assets/logo1.png')}
            style={{
              width: '100%',
              maxWidth: '100%',
            }}
          />
          <Text style={{ textAlign: 'center', color: 'white' }}>
            where the Job finds YOU!
          </Text>

          <Text style={style.h1}>Sign In</Text>

          {this.state.err ? (
            <FormValidationMessage>
              {this.state.err.message ? this.state.err.message : this.state.err}{' '}
            </FormValidationMessage>
          ) : (
            <View />
          )}
          <View style={{ alignItems: 'center' }}>
            <TextInput
              placeholder="Username"
              underlineColorAndroid="transparent"
              style={style.loginInut}
              onChangeText={value => this.onChangeText('username', value)}
            />
            <TextInput
              style={style.loginInut}
              placeholder="Password"
              onChangeText={value => this.onChangeText('password', value)}
              secureTextEntry
            />
          </View>
          <Button
            backgroundColor="#4cda64"
            buttonStyle={style1.button}
            title="Let's Go"
            onPress={this.signIn.bind(this)}
          />
          <View
            style={{
              flexDirection: 'row',
            }}
          >
            <View style={style1.wrapButtons}>
              <Text
                style={style1.bottomButtons}
                onPress={() => this.props.navigation.navigate('SignUP')}
              >
                Create account
              </Text>
            </View>
            <View style={style1.wrapButtons}>
              <Text
                style={style1.bottomButtons}
                onPress={() => this.props.navigation.navigate('Forgot')}
              />
            </View>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
          }}
        >
          <View style={style1.wrapButtons}>
            <Button
              backgroundColor="#ea4335"
              buttonStyle={style1.button}
              icon={{ name: 'google-plus-square', type: 'font-awesome' }}
              title="Sign up With Google"
              onPress={this._handlePressGoogle}
            />
          </View>
          <View style={style1.wrapButtons}>
            <Button
              backgroundColor="#3B5998"
              buttonStyle={style1.button}
              icon={{ name: 'facebook-square', type: 'font-awesome' }}
              title="Sign up With Facebook"
              onPress={this._handlePressFacebook}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default withNavigation(SignINScreen);
const style1 = {
  wrapButtons: { width: '50%' },
  bottomButtons: {
    paddingTop: 20,
    paddingBottom: 20,
    color: 'white',
    textAlign: 'center',
    fontSize: 12,
    textDecorationLine: 'underline',
  },
  button: {
    borderRadius: 10,
    marginBottom: 10,
  },
  container: {
    bottom: 0,
    flex: 1,
    height: '100%',
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    width: '100%',
  },
};
