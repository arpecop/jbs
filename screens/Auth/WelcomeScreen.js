import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, AsyncStorage } from 'react-native';
import { Button, Divider } from 'react-native-elements';

import { NavigationActions } from 'react-navigation';

// 1960300914279891

export default class WelcomeScreen extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super();
    this.state = {
      auth: {},
      background: '0',
      validation: {
        email: '',
        password: '',
      },
    };
  }
  _handlePressFacebook = async () => {
    const {
      type,
      token,
      expires,
    } = await Expo.Facebook.logInWithReadPermissionsAsync('1960300914279891', {
      permissions: ['public_profile'],
    });
    if (type === 'success') {
      // sign in with federated identity
      Auth.federatedSignIn(
        'facebook',
        { token, expires_at: expires },
        { name: 'USER_NAME' },
      )
        .then((credentials) => {
          console.log('get aws credentials', credentials);
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };

  _handlePressGoogle = async () => {
    const setAuth = await AsyncStorage.setItem(
      'auth',
      JSON.stringify({ first_name: 'Rudi' }),
    );
    // await AsyncStorage.setItem('userToken', 'abc');
    // this.props.navigation.navigate('App');
  };
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Image
          source={require('../../assets/back.jpg')}
          style={styles.container}
        />

        <Image
          resizeMode="contain"
          source={require('../../assets/logo.png')}
          style={{
            top: 20,
            width: '100%',
            maxWidth: '100%',
          }}
        />
        <Text style={{ textAlign: 'center' }}>Where the JOB finds YOU!</Text>

        <Divider style={{ backgroundColor: 'blue' }} />
        <Button
          large
          backgroundColor="#34495e"
          buttonStyle={styles.button}
          icon={{ name: 'envelope', type: 'font-awesome' }}
          title="Sign up With Email"
          onPress={() => this.props.navigation.navigate('SignIN')}

          // onPress={this._emailSignup}
        />
      </View>
    );
  }
}

var styles = StyleSheet.create({
  button: {
    borderRadius: 10,
    marginBottom: 10,
    width: 400,
  },
  container: {
    bottom: 0,
    flex: 1,
    height: '100%',
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    width: '100%',
  },
});
