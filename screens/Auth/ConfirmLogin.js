import React, { Component } from 'react';
import { View, AsyncStorage } from 'react-native';

import { Button, Divider } from 'react-native-elements';
import Amplify, { Auth } from 'aws-amplify';
import {
  FormLabel,
  FormInput,
  FormValidationMessage,
} from 'react-native-elements';
import AWSConfig from '../../aws-exports';

import { style } from '../../constants/styles';

import { amplifyConfig } from '../../constants/amplifyConfig';


Amplify.configure(AWSConfig);

const title = 'Confirmation Code';
Amplify.configure(amplifyConfig);

export default class SignUPscreen extends React.Component {
  static navigationOptions = {
    title,
    ...style.header,
  };
  state = {
    confirmationCode: '',
    err: false,
  }
  onChangeText(key, value) {
    this.setState({
      [key]: value,
    });
  }

  confirmSignUp() {
 
    
    Auth.confirmSignUp(this.props.navigation.state.params.username, this.state.confirmationCode)
      .then(() => {
        this.props.navigation.navigate('Home', {
          username: this.state.username,

        }); 
        AsyncStorage.setItem('userToken',this.state.username);
      })
      .catch(err => console.log('error confirming signing up!: ', err));
  }

  render() {
    //   <Authenticator />

    return (<View>

      {this.state.err ? (
        <FormValidationMessage>{this.state.err.message ? this.state.err.message : this.state.err} </FormValidationMessage>

      ) : (
        <View />

      )}
      <FormLabel>Code:</FormLabel>
      <FormInput
        onChangeText={value => this.onChangeText('confirmationCode', value)}


      />

      <Button title="Confirm Sign Up" onPress={this.confirmSignUp.bind(this)} />


            </View>);
  }
}

