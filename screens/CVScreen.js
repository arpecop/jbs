import React, { Component } from 'react';
import { style } from '../constants/styles';

import HeaderTop from '../components/Header';
import Bottom from '../components/Footer';
import { Container, Content } from 'native-base';

const title = 'Profile';


export default class CV extends Component {
  static navigationOptions = {
    title,
    ...style.header,
  };

  render() {
    return (
      <Container>
        <HeaderTop title={title} backButton />
        <Content />
        <Bottom />
      </Container>
    );
  }
}
