import React, { Component } from 'react';
import { style } from '../constants/styles';

import HeaderTop from '../components/Header';
import Bottom from '../components/Footer';
import { Container, Content, Text } from 'native-base';
import { Constants, Calendar } from 'expo';

const title = 'Job Calendar';


export default class JobCalendar extends Component {
  static navigationOptions = {
    title,
    ...style.header,
  };

  render() {
    return (
      <Container>
        <HeaderTop title={title} backButton />
        <Content>
          <Text>Calendar Screen</Text>
        </Content>
        <Bottom />
      </Container>

    );
  }
}
