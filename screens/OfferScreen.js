import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Constants, MapView, Location, Permissions } from 'expo';
import SafeAreaView from 'react-native-safe-area-view';
import { style } from '../constants/styles';


export default class Offer extends Component {
  static navigationOptions = {
    header: null,
    ...style.header,
  };

  render() {
    return (
      <SafeAreaView forceInset={{ top: 'always' }} style={{ flex: 1 }}>
        <View>
          <Text> Offer & apply </Text>
        </View>
      </SafeAreaView>
    );
  }
}

