import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import { Constants, MapView, Location, Permissions } from "expo";
import { SearchBar } from "react-native-elements";
import SafeAreaView from "react-native-safe-area-view";
import { style } from "../constants/styles";

export default class Search extends Component {
  static navigationOptions = {
    header: null,
    ...style.header
  };
  state = {
    mapRegion: null,
    hasLocationPermissions: false,
    locationResult: null
  };

  componentDidMount() {
    this._getLocationAsync();
  }

  _handleMapRegionChange = mapRegion => {
    console.log(mapRegion);
    this.setState({ mapRegion });
  };

  _getLocationAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.setState({
        locationResult: "Permission to access location was denied"
      });
    } else {
      this.setState({ hasLocationPermissions: true });
    }

    const location = await Location.getCurrentPositionAsync({});

    this.setState({ locationResult: JSON.stringify(location) });

    // Center the map on the location we just fetched.
    this.setState({
      mapRegion: {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
      }
    });
  };
  render() {
    return (
      <SafeAreaView forceInset={{ top: "always" }} style={{ flex: 1 }}>
        <View>
          <SearchBar lightTheme placeholder="Type Here..." />
        </View>
        <View style={styles.container}>
          {this.state.locationResult === null ? (
            <Text>Finding your current location...</Text>
          ) : this.state.hasLocationPermissions === false ? (
            <Text>Location permissions are not granted.</Text>
          ) : this.state.mapRegion === null ? (
            <Text>Map region doesn't exist.</Text>
          ) : (
            <MapView
              style={{ alignSelf: "stretch", height: 400,flex: 1  }}
              region={this.state.mapRegion}
              onRegionChange={this._handleMapRegionChange}
            />
          )}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: Constants.statusBarHeight,
    backgroundColor: "#ecf0f1"
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
    color: "#34495e"
  }
});
