import React, { Component } from 'react';
import { style } from '../constants/styles';
import { Listx } from '../components/List';
import HeaderTop from '../components/Header';
import Bottom from '../components/Footer';
import myTheme from '../constants/theme';
import {
  Container,
  Content,
  Button,
  Icon,
  Text,
  SideBar,
  Drawer,
} from 'native-base';

const title = 'My Offers';
export default class AllOffers extends Component {
  static navigationOptions = {
    title,
    ...style.header,
    headerRight: (
      <Icon
        name="md-menu"
        style={{ color: 'white', marginRight: 10 }}
        onPress={() => this.closeDrawer()}
      />
    ),
  };
  render() {
    closeDrawer = () => {
      this.drawer._root.close();
    };
    openDrawer = () => {
      this.drawer._root.open();
    };
    return (
      <Container>
        <HeaderTop title={title} backButton />
        <Content theme={myTheme}>
          <Listx
            query="server"
            endpoint="search_position"
            navigation={this.props.navigation}
          />
          <Drawer
            ref={(ref) => {
              this.drawer = ref;
            }}
            content={<Text>dsad</Text>}
            onClose={() => this.closeDrawer()}
          />
        </Content>
        <Bottom />
      </Container>
    );
  }
}
