import React, { Component } from 'react';
import { style } from '../constants/styles';
import { Listx } from '../components/List';
import HeaderTop from '../components/Header';
import Bottom from '../components/Footer';
import { Container, Content, Header, Segment, Button } from 'native-base';
import { ScrollView, Image, View, Text, Tab } from 'react-native';
import { get } from '../api/';

const title = 'Other Offers';
export default class AllOffers extends Component {
  static navigationOptions = {
    headerTitle: (
      <Image source={require('../assets/logo.png')} />
    ),
    ...style.header,
  };
  state = {
    searchterm: {
      endpoint: '',
      query: '',
      limit: '500',
    },
    cats: { rows: [] },
  };
  async componentWillMount() {
    // get(endpoint, key, limit) {
    const cats = await get('cats');
    console.log(cats.rows);
    this.setState({ cats: { rows: cats.rows } });
  }
  render() {
    return (
      <Container>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
          style={{ position: 'absolute', top: 0, zIndex: 1 }}
        >
          {this.state.cats.rows.map(data => (
            <View
              key={data.key}
              style={{
                  width: 130,
                  height: 120,
                  borderWidth: 0.5,
                  margin: 5,
                  borderColor: 'gray',
                  backgroundColor: 'white',
                }}
            >

              <View style={{ flex: 1, padding: 5 }}><Text>{data.value.title}</Text></View>
            </View>

          ))}
        </ScrollView>
        <Content style={{ marginTop: 125 }}>
          <Listx endpoint="search_all" navigation={this.props.navigation} />
        </Content>
        <Bottom />
      </Container>
    );
  }
}
