import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';

import { Font, AppLoading } from 'expo';
import { createSwitchNavigator, createStackNavigator } from 'react-navigation';
import AllOffers from './screens/AllOffersScreen';
import CV from './screens/CVScreen';
import Home from './screens/HomeScreen';
import Calendar from './screens/JobCalendarScreen';
import MyJobs from './screens/MyJobsScreen';
import MyOffers from './screens/MyOffersScreen';
import Reviews from './screens/ReviewsScreen';
import Search from './screens/SearchScreen';
import Offer from './screens/OfferScreen';
// LOGIN
import SignINScreen from './screens/Auth/SignInScreen';
import SignUP from './screens/Auth/SignUpScreen';
import ForgotPassword from './screens/Auth/ForgotPassword';
import ConfirmLogin from './screens/Auth/ConfirmLogin';
// DRAWER
import Test from './screens/Test';

import { get } from './api/';

class AuthLoadingScreen extends React.Component {
  async componentWillMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      Roboto_light: require('./assets/Roboto-Light.ttf'),
    });
    this._bootstrapAsync();
  }
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userToken');
    this.props.navigation.navigate(userToken ? 'AllOffers' : 'SignIN'); // App
  };

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
// createDrawerNavigator
const MyApp = createStackNavigator({
  Home,
  SignIN: SignINScreen,
  SignUP,
  Forgot: ForgotPassword,
  AllOffers,
  MyOffers,
  CV,
  Reviews,
  Calendar,
  MyJobs,
  Search,
  Test,
  Offer,
  ConfirmLogin,
});

export default createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    MyApp,
  },
  {
    initialRouteName: 'AuthLoading',
  },
);
