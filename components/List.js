import React from 'react';

import { List, ListItem, Text, Thumbnail, Body } from 'native-base';

import { Loading } from '../components/Loading';
import shuffle from 'lodash/shuffle';

import { get } from '../api/';

export class Listx extends React.Component {
  constructor() {
    super();
    this.state = {
      arr: [],
      phrases: ['wants to hire', 'is looking for'],
      isReady: false,
      modalVisible: false,
      item: {
        _id: '111111sss111x',
        _rev: '3-239ebca1d57dc24285985bdac0cbabed',
        city: 'Varna',
        image:
          'https://pbs.twimg.com/profile_images/971989448765272064/G8w6NeDX.jpg',
        country: 'Bulgaria',
        company: 'Hotel Rodina',
        type: 'seasonal',
        fromDate: '',
        toDate: '',
        industry: ['hotels'],
        position: 'piccolo',
        description: 'Lorem Ipsum',
        languages: ['EN', 'DE'],
      },
    };
  }

  async componentWillMount() {
    const res = await get(
      this.props.endpoint,
      this.props.query,
      this.props.limit,
    );

    this.setState({ arr: res.rows, isReady: true });
  }

  render() {
    const resizeMode = 'cover';
    if (!this.state.isReady) {
      return <Loading />;
    }
    return (
      <List>
        {this.state.arr.map(data => (
          <ListItem
            onPress={() => this.props.navigation.navigate('Offer')}
            key={data.key}
          >
            <Thumbnail circle size={100} source={{ uri: data.value.image }} />
            <Body>
              <Text>
                {data.value.company} {shuffle(this.state.phrases)[0]}{' '}
                {data.value.position}
              </Text>
              <Text numberOfLines={1} ellipsizeMode="tail" note>
                {data.value.description}
              </Text>
            </Body>
          </ListItem>
        ))}
      </List>
    );
  }
}
