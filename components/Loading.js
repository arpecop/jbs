import React from 'react';
import { Button } from 'react-native-elements';
import { View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export class Loading extends React.Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Button
          color="blue"
          loading

          size={55}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}

        />
      </View>
    );
  }
}
