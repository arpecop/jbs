import React from 'react';
import { AppRegistry, Image, StatusBar } from 'react-native';
import { Container, Content, Text, List, ListItem, Left, Right, Icon } from 'native-base';
import data from '../constants/data.json';

export default class SideBar extends React.Component {
  render() {
    const Items = Object.values(data.routes).filter((item) => {
      if (item.drawer) {
        return item;
      }
    });
    return (
      <Container>
        <Content>
          <List>
            {Object.values(Items).map((value, i) => (
              <ListItem
                key={i}
                icon
                onPress={() =>
                      this.props.navigation.navigate(value.destination)
                    }
              >
                <Left>
                  <Icon name={value.icon} />
                </Left>
                <Right>
                  <Text>{value.text}</Text>
                </Right>
              </ListItem>
            ))}
          </List>
        </Content>
      </Container>
    );
  }
}
