import React from 'react';
import { withNavigation } from 'react-navigation';
import myTheme from '../constants/theme';
import { Footer, FooterTab, Button, Icon, Text, Content } from 'native-base';
import data from '../constants/data.json';

class Bottom extends React.Component {
  render() {
    const Items = Object.values(data.routes).filter((item) => {
      if (item.footertab) {
        return item;
      }
    });
    return (

      <Footer theme={myTheme}>
          {Object.values(Items).map((value, i) => (
            <FooterTab key={value.destination}>
              <Button
                vertical
                onPress={() => this.props.navigation.navigate(value.destination)}
              >
                <Icon name={value.icon} />
                <Text>{value.text}</Text>
              </Button>
            </FooterTab>
        ))}
        </Footer>

    );
  }
}

export default withNavigation(Bottom);
