import React from 'react';
import { withNavigation, NavigationActions } from 'react-navigation';
import { Constants } from 'expo';
import {
  Header,
  Title,
  Button,
  Left,
  Right,
  Body,
  Subtitle,
  Icon,
  Text,
  View,
} from 'native-base';

// class ThemeHeader extends React.Component {
class HeaderTop1 extends React.Component {
  closeDrawer = () => {
    this.drawer._root.close();
  };
  openDrawer = () => {
    this.drawer._root.open();
  };
  render() {
    const { navigate } = this.props.navigation;
    const styleTitle = { fontFamily: 'Roboto_light', fontSize: 21 };
    const Titlex = this.props.subtitle ? (
      <Body>
        <Title style={styleTitle}>
          <Text>{this.props.title}</Text>
        </Title>
        <Subtitle>{this.props.subtitle}</Subtitle>
      </Body>
    ) : (
      <Body>
        <Title style={styleTitle}>{this.props.title}</Title>
      </Body>
    );
    const backAction = NavigationActions.back({
      key: null,
    });

    const backButton = this.props.backButton ? (
      <Left>
        <Button
          // onPress={() => this.props.navigation.navigate('SignIN')}
          onPress={() => navigate(NavigationActions.back())}
          transparent
        >
          <Icon name="arrow-back" />
        </Button>
      </Left>
    ) : (
      <View />
    );
    const bgColor = '#6c5ce7';

    return (
      <View
        style={{
          backgroundColor: bgColor,
          paddingTop: Constants.statusBarHeight,
        }}
      >
        <Header style={{ backgroundColor: bgColor }}>
          {backButton}
          {Titlex}
          <Right>
            <Button
              // onPress={() => this.openDrawer}
              // onPress={() => this.props.navigation.navigate('DrawerToggle')}

              onPress={() => this.props.navigation.navigate('DrawerOpen')}
              transparent
            >
              <Icon name="menu" />
            </Button>
          </Right>
        </Header>
      </View>
    );
  }
}

class HeaderTop extends React.Component {
  render() {
    return <View />;
  }
}

export default withNavigation(HeaderTop);
