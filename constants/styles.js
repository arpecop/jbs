import { StyleSheet } from 'react-native';

const style = {
  header: {
    headerTitleStyle: {
      color: 'white', textAlign: 'center', alignSelf: 'center', fontFamily: 'Roboto_light',
    },
    headerStyle: {
      backgroundColor: '#10142a',
      shadowOpacity: 0,
      shadowOffset: {
        height: 0,
      },
      shadowRadius: 0,
    },
    headerTintColor: 'white',
  },
  headerWrapper: {
    borderWidth: StyleSheet.hairlineWidth,
  },
  h1: {
    textAlign: 'center',
    fontSize: 30,
    marginBottom: 5,
    color: 'white',
    fontFamily: 'sans-serif-light',
  },
  badge: {
    position: 'absolute',
    zIndex: 1,
  },
  parent: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  child: {
    width: '31.333333%',
    margin: '1%',
    aspectRatio: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginInut: {
    borderColor: 'rgba(0, 0, 0, 0.5);',
    backgroundColor: 'rgba(0, 0, 0, 0.5);',
    width: '90%',
    padding: 5,
    fontFamily: 'Roboto_light',
    color: 'white',
    borderWidth: 1,
    borderStyle: 'solid',
    fontSize: 15,
    borderRadius: 25,
    marginBottom: 10,
  },
  GreenButton: {
    borderRadius: 10,

    marginBottom: 10,
  },
};

module.exports = {
  style,
};

