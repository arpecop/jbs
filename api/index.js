import { AsyncStorage } from 'react-native';
import Amplify, { API } from 'aws-amplify';

Amplify.configure({
  API: {
    endpoints: [
      {
        name: 'MyAPIGatewayAPI',
        endpoint: 'http://arpecop.com/jobsio/_design/offers/_view/',
      },
    ],
  },
});

export async function get1(pathx, params) {
  const apiName = 'MyAPIGatewayAPI';
  const path = `/${pathx}`;
  const myInit = { // OPTIONAL
    headers: {}, // OPTIONAL
    response: true,
    queryStringParameters: {}, // OPTIONAL
  };
  API.get(apiName, path, myInit).then((response) => {
    // Add your code here
    console.log(response);
  }).catch((error) => {
    console.log(error.response);
  });
}


export async function get(endpoint, key, limit) {
  const url = `http://arpecop.com/jobsio/_design/offers/_view/${endpoint}${limit ? `?limit=${limit}` : '?limit=500'}${key ? `&key="${key}"` : ''}`;
  console.log(url);

  return new Promise((resolve) => {
    fetch(url)
      .then(response => response.json())
      .then(responseJson => resolve(responseJson))
      .catch((error) => {
        resolve(error);
      });
  });
}


export async function userUpdate(params) {
  const user = await AsyncStorage.getItem(params._id);
}
